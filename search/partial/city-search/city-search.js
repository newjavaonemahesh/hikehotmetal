angular.module('search').controller('CitySearchCtrl',function($scope){

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    $scope.city_search = {};

    $scope.city_search.inlineOptions = {
        customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
    };

    $scope.city_search.dateOptions = {
        dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
    };

    $scope.city_search.toggleMin = function() {
        $scope.city_search.inlineOptions.minDate = $scope.city_search.inlineOptions.minDate ? null : new Date();
        $scope.city_search.dateOptions.minDate = $scope.city_search.inlineOptions.minDate;
    };

    $scope.city_search.toggleMin();


    $scope.city_search.open1 = function() {
        $scope.city_search.popup1.opened = true;
    };


    $scope.city_search.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.city_search.format = $scope.city_search.formats[0];
    $scope.city_search.altInputFormats = ['M!/d!/yyyy'];
    $scope.city_search.popup1 = {
        opened : false
    };


});