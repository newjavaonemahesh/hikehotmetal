angular.module('hikehotmetal', ['ui.bootstrap', 'ui.router', 'ngAnimate', 'google.places', 'textAngular', 'satellizer', 'search', 'admin', 'sidebar', 'page', 'auth', 'post', 'shared', 'home', 'widget']);

angular.module('hikehotmetal').config(function($stateProvider, $urlRouterProvider, $locationProvider, $authProvider) {

    $stateProvider
        .state('parent', {
            //url: '',
            abstract: true,
            template: '<div ui-view></div>',
            controller: 'AppCtrl'
        })
        .state('parent.page', {
            //url: '',
            abstract: true,
            views: {
                '@parent':{
                    templateUrl: 'page/partial/index/index.html'
                },
                'header@parent.page': {templateUrl: 'page/partial/index/header.html'},
                'footer@parent.page': {templateUrl: 'page/partial/index/footer.html'}
            }
        })
        .state('parent.page.home', {
            url: '/',
            views: {
                'content@parent.page': {templateUrl: 'page/partial/home/home.html'}
            }
        }

    );

    $authProvider.loginUrl = 'http://hikehotmetal.app/authenticate';
    $authProvider.withCredentials = false;

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

    $urlRouterProvider.rule(function($injector, $location) {

        var path = $location.path();
        var hasTrailingSlash = path[path.length-1] === '/';

        if(hasTrailingSlash) {

            //if last charcter is a slash, return the same url without the slash
            var newPath = path.substr(0, path.length - 1);
            return newPath;
        }

    });

    /* Add New States Above */
    $urlRouterProvider.otherwise('', '/');

});


angular.module('hikehotmetal').controller('AppCtrl', function ($scope){
    console.log('inside AppCtrl');
});


angular.module('hikehotmetal').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});
