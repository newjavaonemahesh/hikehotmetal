angular.module('auth', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('auth').config(function($stateProvider) {


    $stateProvider
        .state('parent.page.auth', {
            url: '/auth',
            views: {
                'content@parent.page': {templateUrl: 'auth/auth.html'}
            }
        })
        .state('parent.page.users', {
            url: '/users',
            views: {
                'content@parent.page': {templateUrl: 'auth/users.html'}
            }
        })
    ;
    /* Add New States Above */

});

angular.module('auth').controller('AuthController', function ($scope, $auth, $state) {
    console.log('inside AuthController');

    $scope.login = function() {

        var credentials = {
            email: $scope.email,
            password: $scope.password
        };

        // Use Satellizer's $auth service to login
        $auth.login(credentials).then(function(data) {

            // If login is successful, redirect to the users state
            $state.go('parent.page.users', {});
        });
    };

});

angular.module('auth').controller('UserController', function ($scope, $http) {

    $scope.getUsers = function() {

        // This request will hit the index method in the AuthenticateController
        // on the Laravel side and will return the list of users
        $http.get('http://hikehotmetal.app/authenticate').success(function(users) {
            $scope.users = users;
        }).error(function(error) {
            $scope.error = error;
        });
    };

});
