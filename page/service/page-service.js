angular.module('page').factory('pageService',function($http, transformRequestAsFormPost) {

    var pageService = {
        addPage: function(page) {
            var request = $http({
                method: "post",
                url: 'http://hikehotmetal.app/page',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: transformRequestAsFormPost.transformRequest,
                data: page
            });
            return request;
        }
    };

    return pageService;
});

