angular.module('page', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate']);

angular.module('page').config(function($stateProvider) {


    $stateProvider
        .state('parent.admin.pages', {
            url: '/pages',
            abstract: true,
            views: {
                'content@parent.admin': {template: '<div ui-view="pagesContent"></div>'}
            }
        })
        .state('parent.admin.pages.list', {
            url: '/list',
            views: {
                'pagesContent@parent.admin.pages': {templateUrl: 'page/partial/page/pageList.html'}
            }
        })
        .state('parent.admin.pages.new', {
            url: '/new',
            views: {
                'pagesContent@parent.admin.pages': {templateUrl: 'page/partial/page/pageNewAdd.html'}
            }
        })
        .state('parent.admin.pages.appearance', {
            url: '/appearance',
            abstract: true
        })
        .state('parent.admin.pages.appearance.widgets', {
            url: '/widgets',
            views: {
                'pagesContent@parent.admin.pages': {templateUrl: 'page/partial/admin/widget/widgets.html'}
            }
        });

    /* Add New States Above */

});



