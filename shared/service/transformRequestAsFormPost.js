(function(Object, services) {
    'use strict';

    // the factory to expose that allows the creation of application instances
    var transformRequestAsFormPostFactory = function() {
        console.log("Application factory!");

        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        var param = function(obj) {
            var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

            for(name in obj) {
                value = obj[name];

                if(value instanceof Array) {
                    for(i=0; i<value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if(value instanceof Object) {
                    for(subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if(value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        return {
            transformRequest: function(data, getHeaders) {
                var headers = getHeaders();
                // Use x-www-form-urlencoded Content-Type
                headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";

                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;

            }
        };
    };

    services.factory('transformRequestAsFormPost', [transformRequestAsFormPostFactory]);
})(Object, angular.module('shared'));