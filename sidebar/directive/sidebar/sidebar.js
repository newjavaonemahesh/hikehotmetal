angular.module('sidebar').directive('sidebar', function($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        scope: {

        },
        templateUrl: 'sidebar/directive/sidebar/sidebar.html',
        link: function(scope, element, attrs, fn) {

            $(window).resize(function() {
                var path = $(this);
                var contW = path.width();
                if(contW >= 751){
                    document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
                    //scope.$emit('changeDashboardMargin', '200px');
                    $rootScope.$broadcast('changeDashboardMargin', '200px');
                }else{
                    document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
                    //scope.$emit('changeDashboardMargin', '0px');
                    $rootScope.$broadcast('changeDashboardMargin', '0px');
                }

            });

            $('.dropdown').on('show.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
            });
            $('.dropdown').on('hide.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
            });
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                var elem = document.getElementById("sidebar-wrapper");
                var left = window.getComputedStyle(elem,null).getPropertyValue("left");
                if(left === "200px"){
                    document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
                    //document.getElementById("dashboard-content").style.left="0px";
                    $rootScope.$broadcast('changeDashboardMargin', '0px');
                }
                else if(left === "-200px"){
                    document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
                    //document.getElementById("dashboard-content").style.left="200px";
                    $rootScope.$broadcast('changeDashboardMargin', '200px');
                }



            });


        }
    };
});
