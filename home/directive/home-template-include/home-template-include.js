angular.module('home').directive('homeTemplateInclude', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            optionsData: '=optionsData'
        },
        template: '<ng-include src="getTemplateUrl()"/>',
        link: function (scope, element, attrs, fn) {


        },
        controller: function ($scope) {
            //function used on the ng-include to resolve the template
            $scope.getTemplateUrl = function () {
                //basic handling
                if ($scope.optionsData.home_page === "home") {
                    return "home/partial/home-include/home-include.html";
                }
            };
        }
    };
});
