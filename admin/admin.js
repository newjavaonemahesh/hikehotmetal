angular.module('admin', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('admin').config(function($stateProvider) {


    $stateProvider
        .state('parent.admin', {
            url: '/admin',
            abstract: true,
            views: {
                '@parent': {templateUrl: 'page/partial/admin/index/index.html'},
                'header@parent.admin': {templateUrl: 'page/partial/admin/index/header.html'},
                'left@parent.admin': {templateUrl: 'page/partial/admin/index/sidebar.html'},
                'content@parent.admin': {template: '<div ui-view="adminContent"></div>'}
            }
        })
        .state('parent.admin.dashboard', {
            url: '/dashboard',
            views: {
                'adminContent@parent.admin': {templateUrl: 'page/partial/admin/dashboard/dashboard.html', controller: 'DashboardCtrl'}
            }
        });


    /* Add New States Above */

});







